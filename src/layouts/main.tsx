import { PropsWithChildren, useEffect } from 'react'

export default function Layout(props: PropsWithChildren<{}>) {
    useEffect(() => {
        console.log('redner main layout')
    }, [])
    return (
        <div>
            <div>Layout Default</div>
            {props.children}
        </div>
    )
}
