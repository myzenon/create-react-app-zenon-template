export default {
    main: () => import('@/layouts/main'),
} as { [key: string]: () => Promise<any> }
