import { useState, PropsWithChildren, useEffect } from 'react'
import { Route as RouterRoute, useHistory } from 'react-router-dom'
import loadable from '@loadable/component'
import middlewares from '@/middlewares'
import { RouteConfig, MiddlewareContext, Middleware } from '@/types/framework'
import { useStores } from '@/stores'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'

export default function generateRoute({ component: dynamicImportComponent, ...config }: RouteConfig) {
    let isBlockRendering = false
    const Component = loadable(dynamicImportComponent, {
        fallback: <div>Now Loading...</div>,
    })

    function Route(props: PropsWithChildren<any>) {
        useState(nprogress.start())
        const [ isProcessing, setIsProcessing ] = useState(config.middlewares && Array.isArray(config.middlewares) && config.middlewares.length > 0 ? true : false)
        const history = useHistory()
        const stores = useStores()

        const redirect = (path: string) => {
            isBlockRendering = true
            history.replace(path)
        }

        // Run Middlewares
        useEffect(() => {
            isBlockRendering = false
            let requestedMiddlewares: Middleware[] = []
            if (config.middlewares && Array.isArray(config.middlewares)) {
                requestedMiddlewares = config.middlewares.map(middlewareName => middlewares[middlewareName]).filter(middleware => !!middleware)
            }
            const context: MiddlewareContext = {
                history,
                redirect,
                stores,
            }
            Promise.all(requestedMiddlewares.map(middleware => middleware(context)))
                .then(() => {
                    if (!isBlockRendering) {
                        setIsProcessing(false)
                        nprogress.done()
                    }
                })
        }, [])

        if (isProcessing) {
            return <div>Processing ...</div>
        }

        return (
            <Component {...props} />
        )
    }

    return (
        <RouterRoute key={config.path} {...config}>
            <Route />
        </RouterRoute>
    )
}
