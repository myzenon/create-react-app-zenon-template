import { Plugin } from '@/types/framework'
import mock from './mock'

export default [
    mock,
] as Plugin[]
