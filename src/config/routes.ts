import { RouteConfig } from '@/types/framework'

export default [
    {
        path: '/home',
        component: () => import('@/pages/Home'),
        middlewares: [ 'waitTime' ],
        layout: 'main',
    },
    {
        path: '/login',
        component: () => import('@/pages/Login'),
        layout: 'main',
    },
    {
        path: '/test',
        component: () => import('@/pages/Test'),
        middlewares: [ 'auth' ],
        layout: 'main',
    },
] as RouteConfig[]
