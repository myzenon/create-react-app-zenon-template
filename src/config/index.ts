import merge from 'lodash.merge'
import { FrameworkConfig } from '@/types/framework'

const environment = process.env['REACT_APP_ENV'] ?? 'local'

const config = {
    environment,
    apiEndpoint: 'https://somebackend.com',
}

const envConfig = {
    local: {
    },
    dev: {
    },
    staging: {
    },
    prod: {
    },
} as { [key: string]: FrameworkConfig }

const runtimeConfig = {
    apiEndpoint: process.env['REACT_APP_API_ENDPOINT'],
}

const mergeConfig = (baseConfig: FrameworkConfig, replaceConfig: FrameworkConfig): void => {
    Object.keys(baseConfig).map((key) => {
        if (replaceConfig[key] !== undefined) {
            if (baseConfig[key] instanceof Object) {
                baseConfig[key] = merge(baseConfig[key], replaceConfig[key])
            }
            else {
                baseConfig[key] = replaceConfig[key]
            }
        }
    })
}

mergeConfig(config, envConfig[environment])
mergeConfig(config, runtimeConfig)

export default config
