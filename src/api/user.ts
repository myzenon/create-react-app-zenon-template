import api from './instance'

export default {
    getProfile: () => {
        type GetProfileResponse = {
            first_name: string,
            last_name: string,
        }
        return api.axios.get<GetProfileResponse>('/profile')
            .then(response => response.data)
    },
}
