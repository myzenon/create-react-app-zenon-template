import axios from 'axios'
import config from '@/config'
import stores from '@/stores'

const instance = {
    axios: axios.create({
        baseURL: config.apiEndpoint,
    }),
}

instance.axios.interceptors.request.use(config => {
    if (stores.auth.isAuthenticated) {
        config.headers.Authorization = `Bearer ${stores.auth.token}`
    }
    return config
})

instance.axios.interceptors.response.use(
    response => response,
    error => {
        // We can handle 404, 500 also network error here.
        throw error
    },
)

export default instance
