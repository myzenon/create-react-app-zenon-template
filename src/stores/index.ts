import Auth from './auth'
import { create } from 'mobx-persist'
import localForage from 'localforage'
import { MobXProviderContext } from 'mobx-react'
import { useContext } from 'react'

const stores = {
    auth: new Auth(),
}

localForage.config({
    driver: localForage.INDEXEDDB,
    name: 'myApp',
    version: 1.0,
    storeName: 'myApp',
    description: 'some description',
})

const hydrate = create({
    storage: localForage,
})

export const reHydrate = () => Promise.all([
    hydrate('auth', stores.auth),
])

export const useStores = () => useContext(MobXProviderContext) as typeof stores

export default stores
