import { makeObservable, observable, computed, action } from 'mobx'
import { persist } from 'mobx-persist'

export default class Auth {
    constructor() {
        makeObservable(this)
    }

    @persist @observable token: string | null = null

    @action setToken(token: string) {
        this.token = token
    }

    @computed get isAuthenticated() {
        return this.token !== null
    }
}
