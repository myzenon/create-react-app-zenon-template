import { MiddlewareContext } from '@/types/framework'

export default async ({ redirect }: MiddlewareContext) => {
    await new Promise((resolve) => {
        setTimeout(() => {
            redirect('/login')
            resolve(1)
        }, 1000)
    })
}
