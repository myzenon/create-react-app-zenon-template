import { Middleware } from '@/types/framework'
import auth from '@/middlewares/auth'
import waitTime from '@/middlewares/waitTime'

export default {
    auth,
    waitTime,
} as { [key: string]: Middleware }
