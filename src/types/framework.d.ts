import { History } from 'history'
import stores from '@/stores'

export type FrameworkConfig = {
    [key: string]: string | object | undefined,
}

export type RouteConfig = {
    path: string,
    component: () => Promise<any>,
    middlewares?: string[],
    layout?: string,
}

export type Middleware = (context: MiddlewareContext) => void | Promise<void>

export type MiddlewareContext = {
    history: History,
    redirect: (path: string) => void,
    stores: typeof stores,
}

export type Plugin = (context: PlugInContext) => void | Promise<void>

export type PlugInContext = {
    stores: typeof stores,
}
