import { Route, Switch } from 'react-router-dom'
import routes from '@/config/routes'
import layouts from '@/layouts'
import generateRoute from '@/components/framework/Route'
import { RouteConfig } from './types/framework'
import loadable from '@loadable/component'

const routesLayouts: { [key: string]: { routes: RouteConfig[], paths: string[] } } = Object.keys(layouts).reduce((obj, layoutName) => ({ ...obj, [layoutName]: { routes: [], paths: [] } }), { default: { routes: [], paths: [] } })
for (const route of routes) {
    if (route.layout && routesLayouts[route.layout]) {
        routesLayouts[route.layout].paths.push(route.path)
        routesLayouts[route.layout].routes.push(route)
    }
    else {
        routesLayouts.default.paths.push(route.path)
        routesLayouts.default.routes.push(route)
    }
}

export default function Router() {
    return (
        <Switch>
            {
                Object.keys(routesLayouts).map(layoutName => {
                    let Routes = (
                        <>
                            {routesLayouts[layoutName].routes.map(route => generateRoute(route))}
                        </>
                    )
                    const Layout = loadable(layouts[layoutName], {
                        fallback: <div>Now Loading...</div>,
                    })
                    if (Layout) {
                        Routes = (
                            <Layout>
                                {Routes}
                            </Layout>
                        )
                    }
                    return (
                        <Route key={layoutName} path={routesLayouts[layoutName].paths}>
                            {Routes}
                        </Route>
                    )
                })
            }
        </Switch>
    )
}
