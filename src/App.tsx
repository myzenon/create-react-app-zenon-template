import { useState, useEffect } from 'react'
import { BrowserRouter } from 'react-router-dom'
import Router from '@/Router'
import { Provider as MobXProvider } from 'mobx-react'
import stores, { reHydrate } from '@/stores'
import plugins from '@/plugins'
import { PlugInContext } from './types/framework'
import '@/themes/global.scss'

export default function App() {
    const [ isProcessing, setIsProcessing ] = useState(true)

    // Call Plugins and Rehydrate stores
    useEffect(() => {
        const pluginContext: PlugInContext = {
            stores,
        }
        const loadingPromises: Promise<any>[] = [ ...plugins.map(plugin => Promise.resolve(plugin(pluginContext))), reHydrate() ]
        Promise.all(loadingPromises).then(() => setIsProcessing(false))
    }, [])

    if (isProcessing) {
        return (
            <div>
                App Processing ...
            </div>
        )
    }

    return (
        <MobXProvider {...stores}>
            <BrowserRouter>
                <Router />
            </BrowserRouter>
        </MobXProvider>
    )
}
