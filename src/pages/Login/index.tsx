import { useEffect } from 'react'
import { Link } from 'react-router-dom'

export default function Login() {
    useEffect(() => {
        console.log('login render')
    }, [])
    return (
        <div>
            Page: Login
            <Link to="/home">Home</Link>
            <Link to="/login">Login</Link>
            <Link to="/test">Test</Link>
        </div>
    )
}
