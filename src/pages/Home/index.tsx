import { useEffect } from 'react'
import { Link } from 'react-router-dom'

export default function Home() {
    useEffect(() => {
        console.log('home render')
    }, [])
    return (
        <div>
            Page: Home
            <Link to="/home">Home</Link>
            <Link to="/login">Login</Link>
            <Link to="/test">Test</Link>
        </div>
    )
}
