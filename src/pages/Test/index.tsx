import { useEffect } from 'react'
import { Link } from 'react-router-dom'

export default function Test() {
    useEffect(() => {
        console.log('test render')
    }, [])
    return (
        <div>
            Page: Test
            <Link to="/home">Home</Link>
            <Link to="/login">Login</Link>
            <Link to="/test">Test</Link>
        </div>
    )
}
